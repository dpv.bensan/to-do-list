@extends("templates.template")
@section("title", "Home")
@section("content")

<h1 class="text-center py-5">Welome to your QUEST BOARD, BRAVE HERO</h1>

<div class="row">
	@foreach($todos as $todo)
		<div class="card col-lg-3 py-2">
			<h3>{{ $todo->name }}</h3>
			<p>{{ $todo->body }}</p>
			<p>Status: {{ $todo->status_id }}</p>
			<p>Category: {{ $todo->category_id }}</p>
		</div>
	@endforeach
</div>

@endsection
